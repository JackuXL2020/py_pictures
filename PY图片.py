import easygui
import webbrowser
easygui.msgbox('欢迎使用PY图片，本程序由JackuXL制作，点击“OK”后选择一张图片，即可打开。', 'PY图片 By JackuXL 1.0')
url = easygui.fileopenbox(msg=None, title=None, default='*',
                         filetypes=["*.png", "*.jpg"])
if (url == None):
    easygui.msgbox('请选择图片！', 'PY图片 By JackuXL 1.0')
else:
    webbrowser.open_new_tab(url)
